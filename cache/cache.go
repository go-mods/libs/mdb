// The cache package provides a simple map with properties of a
// cache (i.e. a certain duration at which key/value pairs are
// removed).
package cache

import (
	"fmt"
	"sync"
	"time"
)

// Cache offers similiar functionality to a map in the sense that it
// stores data in pairs of string keys and []byte values. Any data
// stored in a Cache is deleted after a configurable duration. Note
// that a Cache must be initialized with Init before it can be used.
type Cache struct {
	// Duration describes the time data is kept in the Cache.
	Duration time.Duration
	dataMap  map[string][]byte
	timerMap map[string]*time.Timer
	mutex    sync.Mutex
}

// Init initializes the underlying maps of the Cache. If the Cache
// was already initialized its previous data is omitted.
func (c *Cache) Init() {
	c.dataMap = make(map[string][]byte)
	c.timerMap = make(map[string]*time.Timer)
}

// Get retreives data from the Cache. Returns the data and true if
// the key existed in the Cache or nil and false otherwise.
func (c *Cache) Get(key string) ([]byte, bool) {
	c.mutex.Lock()
	d, b := c.dataMap[key]
	c.mutex.Unlock()
	return d, b
}

// Del deletes the key and data associated to the given key from the
// Cache. Returns true if the key existed, i.e key and data were
// removed, false otherwise.
func (c *Cache) Del(key string) bool {
	deleted := false
	c.mutex.Lock()

	if _, contains := c.timerMap[key]; contains {
		delete(c.dataMap, key)
		delete(c.timerMap, key)
		deleted = true
	}

	c.mutex.Unlock()
	return deleted
}

// Add adds a new key/data pair to the Cache. If the key already
// existed in the Cache the timer for that key will reset.
func (c *Cache) Add(key string, data []byte) {
	c.mutex.Lock()

	// if a key already exists in the Cache we reset its timer
	if _, contains := c.dataMap[key]; contains {
		t := c.timerMap[key]

		// in case t.Stop() returns false (i.e. the time has expired) the
		// timer will be overwritten below (the previous timer routine will
		// have no op)
		if t.Stop() {
			t.Reset(c.Duration)
			c.dataMap[key] = data // overwrites previous data
			// log.Printf("updated '%s' in cache", key)
			c.mutex.Unlock()
			return
		}
	}

	// save data in cache
	c.dataMap[key] = data

	// create and save new timer for that data
	t := time.NewTimer(c.Duration)
	c.timerMap[key] = t

	// wait for expiration of the timer in its own goroutine
	go func() {
		<-t.C // waits until time is received

		// remove cached data
		c.mutex.Lock()

		// ensures the timer was not overwritten
		if c.timerMap[key] == t {
			delete(c.dataMap, key)
			delete(c.timerMap, key)
			// log.Printf("removed '%s' from cache", key)
		}

		c.mutex.Unlock()
	}()

	// log.Printf("added '%s' to cache", key)
	c.mutex.Unlock()
}

// String representation of a Cache is the string representation of
// its underlying map.
func (c *Cache) String() string {
	return fmt.Sprintf("%v", c.dataMap)
}
