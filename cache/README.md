# Cache

[![codecov](https://codecov.io/gh/./branch/master/graph/badge.svg)](https://codecov.io/gh/.)

The cache package provides a simple map with properties of a
cache (i.e. a certain duration at which key/value pairs are
removed).

## Types

### type [Cache](/cache.go#L16)

`type Cache struct { ... }`

Cache offers similiar functionality to a map in the sense that it
stores data in pairs of string keys and []byte values. Any data
stored in a Cache is deleted after a configurable duration. Note
that a Cache must be initialized with Init before it can be used.

#### func (*Cache) [Add](/cache.go#L59)

`func (c *Cache) Add(key string, data []byte)`

Add adds a new key/data pair to the Cache. If the key already
existed in the Cache the timer for that key will reset.

#### func (*Cache) [Del](/cache.go#L43)

`func (c *Cache) Del(key string) bool`

Del deletes the key and data associated to the given key from the
Cache. Returns true if the key existed, i.e key and data were
removed, false otherwise.

#### func (*Cache) [Get](/cache.go#L33)

`func (c *Cache) Get(key string) ([]byte, bool)`

Get retreives data from the Cache. Returns the data and true if
the key existed in the Cache or nil and false otherwise.

#### func (*Cache) [Init](/cache.go#L26)

`func (c *Cache) Init()`

Init initializes the underlying maps of the Cache. If the Cache
was already initialized its previous data is omitted.

#### func (*Cache) [String](/cache.go#L108)

`func (c *Cache) String() string`

String representation of a Cache is the string representation of
its underlying map.

---
Readme created from Go doc with [goreadme](https://github.com/posener/goreadme)
