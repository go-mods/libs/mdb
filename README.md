# Mdb

[![codecov](https://codecov.io/gh/./branch/master/graph/badge.svg)](https://codecov.io/gh/.)

The package mdb provides a simple implementation of a key/value
database with caching support.

## Types

### type [Mdb](/mdb.go#L18)

`type Mdb struct { ... }`

Mdb is a key/value map with persistent data storage. Values added
to the map are stored in files aswell as read from files on
retreival. Data may be cached for a certain duration. Note that a
Mdb must be opened before it can be used.

#### func (*Mdb) [Add](/mdb.go#L111)

`func (mdb *Mdb) Add(key string, value []byte)`

Add adds the given key/value pair to the Mdb. Data is written
directly to a file. If the key already existed in the Mdb its
data is overwritten. Additionaly the value is cached.

#### func (*Mdb) [Close](/mdb.go#L73)

`func (mdb *Mdb) Close()`

Close closes all files opened by the Mdb. This method should always
be called before an Mdb is disposed of.

#### func (*Mdb) [Con](/mdb.go#L81)

`func (mdb *Mdb) Con(key string) bool`

Con checks wether the key exists in the Mdb and returns true if
it is the case, false otherwise.

#### func (*Mdb) [Del](/mdb.go#L135)

`func (mdb *Mdb) Del(key string) bool`

Del deletes the given key completely from the Mdb (i.e. deleting
its file). Returns true on success, false if the key did not
exist in the Mdb.

#### func (*Mdb) [Get](/mdb.go#L90)

`func (mdb *Mdb) Get(key string) ([]byte, bool)`

Get retreives the data associated to the given key from the Mdb.
If the data is not cached it is loaded directly from its file and
caches it. Returns the retreived data and true on success, nil
and false otherwise.

#### func (*Mdb) [Open](/mdb.go#L40)

`func (mdb *Mdb) Open(path string) (err error)`

Open associates the directory pointed to by path to the Mdb. All
files in this folder are scanned and will be kept open for quick
read/write access. Initializes the Cache of the Mdb. This method
must be called before an Mdb can be used. Returns an error if the
directory or any of the files could not be created/accessed.

#### func (Mdb) [String](/mdb.go#L149)

`func (mdb Mdb) String() string`

String returns a string representation of the map for the file
handles from the Mdb.

## Sub Packages

* [cache](./cache): The cache package provides a simple map with properties of a cache (i.e.

---
Readme created from Go doc with [goreadme](https://github.com/posener/goreadme)
