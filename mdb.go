// The package mdb provides a simple implementation of a key/value
// database with caching support.
package mdb

import (
	"fmt"
	"os"

	"gitlab.com/go-mods/lib/bops"
	"gitlab.com/go-mods/lib/hscd"
	"gitlab.com/go-mods/lib/mdb/cache"
)

// Mdb is a key/value map with persistent data storage. Values added
// to the map are stored in files aswell as read from files on
// retreival. Data may be cached for a certain duration. Note that a
// Mdb must be opened before it can be used.
type Mdb struct {
	// The underlying Cache of the Mdb.
	Cache   cache.Cache
	handles map[string]handle
	path    string
}

// file:  pointer to open data file
// start: start byte position of data in file
type handle struct {
	file  *os.File
	start int64
}

// initial value for jenkins hash algorithm
var jen_init = []byte{0x13, 0x12, 0xA1, 0x01}

// Open associates the directory pointed to by path to the Mdb. All
// files in this folder are scanned and will be kept open for quick
// read/write access. Initializes the Cache of the Mdb. This method
// must be called before an Mdb can be used. Returns an error if the
// directory or any of the files could not be created/accessed.
func (mdb *Mdb) Open(path string) (err error) {
	defer func() { err = catch(recover()) }()
	throw(os.MkdirAll(path, 0775))

	mdb.Cache.Init()
	mdb.handles = make(map[string]handle)
	mdb.path = path

	dir, err := os.ReadDir(path)
	throw(err)

	for _, e := range dir {
		f, err := os.OpenFile(path+"/"+e.Name(), os.O_RDWR, 0665)
		throw(err)

		b := make([]byte, 4)
		_, err = f.ReadAt(b, 0)
		throw(err)

		l := (uint32(b[0]) << 24) | (uint32(b[1]) << 16) | (uint32(b[2]) << 8) | uint32(b[3])
		k := make([]byte, l)

		_, err = f.ReadAt(k, 4)
		throw(err)

		mdb.handles[string(k)] = handle{f, int64(l + 4)}
	}

	return err
}

// Close closes all files opened by the Mdb. This method should always
// be called before an Mdb is disposed of.
func (mdb *Mdb) Close() {
	for _, h := range mdb.handles {
		h.file.Close()
	}
}

// Con checks wether the key exists in the Mdb and returns true if
// it is the case, false otherwise.
func (mdb *Mdb) Con(key string) bool {
	_, contains := mdb.handles[key]
	return contains
}

// Get retreives the data associated to the given key from the Mdb.
// If the data is not cached it is loaded directly from its file and
// caches it. Returns the retreived data and true on success, nil
// and false otherwise.
func (mdb *Mdb) Get(key string) ([]byte, bool) {
	h, contains := mdb.handles[key]

	if contains {
		if dat, contains := mdb.Cache.Get(key); contains {
			return dat, true
		}

		inf, _ := h.file.Stat()
		dat := make([]byte, inf.Size()-h.start)
		h.file.ReadAt(dat, h.start)
		mdb.Cache.Add(key, dat)
		return dat, true
	}

	return nil, false
}

// Add adds the given key/value pair to the Mdb. Data is written
// directly to a file. If the key already existed in the Mdb its
// data is overwritten. Additionaly the value is cached.
func (mdb *Mdb) Add(key string, value []byte) {
	h, contains := mdb.handles[key]

	if !contains {
		hash := bops.Str(hscd.JEN([]byte(key), jen_init))
		f, _ := os.OpenFile(mdb.path+"/"+hash, os.O_CREATE|os.O_RDWR, 0665)

		l := len(key)
		f.WriteAt(bops.FromInt(l), 0)
		f.WriteAt([]byte(key), 4)

		h = handle{f, int64(l + 4)}
		mdb.handles[key] = h
	} else {
		h.file.Truncate(h.start)
	}

	h.file.WriteAt(value, h.start)
	mdb.Cache.Add(key, value)
}

// Del deletes the given key completely from the Mdb (i.e. deleting
// its file). Returns true on success, false if the key did not
// exist in the Mdb.
func (mdb *Mdb) Del(key string) bool {
	if h, contains := mdb.handles[key]; contains {
		h.file.Close()
		os.Remove(mdb.path + "/" + h.file.Name())
		delete(mdb.handles, key)
		mdb.Cache.Del(key)
		return true
	}

	return false
}

// String returns a string representation of the map for the file
// handles from the Mdb.
func (mdb Mdb) String() string {
	return fmt.Sprintf("%v", mdb.handles)
}

// throw is helper method that panics when the given err is not nil.
func throw(err error) {
	if err != nil {
		panic(err)
	}
}

// catch is a helper method that is meant to be used in a defer
// context. Takes any interace as argument but usually expects a
// value retreived with recover. Returns the error if the recovered
// value was an error or nil if it was nil, panics otherwise.
func catch(r interface{}) error {
	if r != nil {
		switch e := r.(type) {
		case error:
			return e
		default:
			panic(r)
		}
	}

	return nil
}
